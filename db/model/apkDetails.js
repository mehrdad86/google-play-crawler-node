const {mongoose} = require('./../mongoose');

let strType = {
    type: String
};

let numType = {
    type: Number
};

let boolType = {
    type: Boolean
};

let apkSchema = mongoose.Schema({
    appId: {
        type: String,
        unique: true
    },
    url: strType,
    title: strType,
    description: strType,
    descriptionHTML: strType,
    summary: strType,
    installs: strType,
    minInstalls: numType,
    score: numType,
    scoreText: strType,
    ratings: numType,
    reviews: numType,
    histogram: { '1': numType, '2': numType, '3': numType, '4': numType, '5': numType },
    price: numType,
    free: boolType,
    currency: strType,
    priceText: strType,
    offersIAP: boolType,
    size: strType,
    androidVersion: strType,
    androidVersionText: strType,
    developer: strType,
    developerId: strType,
    developerEmail: strType,
    developerWebsite: strType,
    developerAddress: strType,
    genre: strType,
    genreId: strType,
    familyGenre: strType,
    familyGenreId: strType,
    icon: strType,
    headerImage: strType,
    screenshots: [{
        type: String
    }],
    video: strType,
    videoImage: strType,
    contentRating: strType,
    contentRatingDescription: strType,
    adSupported: boolType,
    released: strType,
    updated: numType,
    version: strType,
    recentChanges: strType,
    comments: [{
        type: String
    }]

});

let ApkDetails = mongoose.model('ApkDetails',apkSchema, 'ApkDetails');

module.exports = {
    ApkDetails
};