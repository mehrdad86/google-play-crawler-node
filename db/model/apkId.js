const {mongoose} = require('./../mongoose');
const mongoosePaginate = require('mongoose-paginate');

let apkSchema = mongoose.Schema({
    appId:{
        type: String,
        unique: true,
        required: true
    },
    title:{
      type: String
    },
    reviewDate: {
        type: Date,
        required: true,
        default: new Date()
    }
});

apkSchema.plugin(mongoosePaginate);

let ApkId = mongoose.model("ApkId", apkSchema, "ApkId");

module.exports = {
    ApkId
};