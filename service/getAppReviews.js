let gplay = require('google-play-scraper');

let getReviews = function(appId){

    return gplay.reviews({
        appId: appId,
        page: 0,
        sort: gplay.sort.NEWEST
    }).then((res)=>{
        return res;
    },(err)=>{
        return err;
    });
};

module.exports = getReviews;