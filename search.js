const gplay = require('google-play-scraper');
const axios = require('axios');
const sleep = require('sleep');
const _ = require('lodash');
const wordList = require('./db/wordList');

const {ApkId} = require('./db/model/apkId');

let nowDate = new Date();
let size = wordList.length -1;
let i=0;


let getRes = function(){
    return gplay.search({
        term: wordList[i],
        num: 250,
        country: 'ir',
        lang: 'fa'
    }).then((res)=>{
       return res
    },(e)=>{
      Promise.reject(e);
    });
};

let saveToDB = function(res){

    res.forEach(function (app) {
            let newApk = {
                appId: app.appId,
                reviewDate: nowDate
            };
            new ApkId(newApk).save().then((res)=>{
                console.log('app saved');
            },(e)=>{
                console.log('cant save app' + e);
            });
        })
}

let getApp = async function() {
    console.log(wordList[i]);
    try {
        let res = await getRes()
        await saveToDB(res);

        sleep.msleep(8000);

    }catch (e) {
        console.log('error:'+e)
        sleep.msleep(10000);
        getApp();
    }
    if (i<size){
        i++
        getApp();
    } else {
        console.log('END');
    }
};

getApp();





