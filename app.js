const express = require('express');
const config = require('config');

const routes = require('./routes/route');




const app = express();
app.use(express.json());

app.use('/api',routes);

app.listen(config.get("PORT"),()=>{
  console.log('app run on port:'+config.get("PORT"));

});



