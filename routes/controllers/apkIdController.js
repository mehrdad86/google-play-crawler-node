const _ = require('lodash');
const queue = require('queue');

const {ApkId} = require('./../../db/model/apkId');


let deleteApp = async function(req, res){

    let appId = req.params.appId;
    try {
        await ApkId.findOneAndDelete({
            appId: appId
        });

        res.status(200).json({
            message: 'app remove from database'
        })


    }catch (e) {
        res.status(404).json({
            message: 'somting went wrong'+e
        })
    }
};

let createApp = async function(req, res){
    let body = _.pick(req.body,['appId']);
    console.log(body);

    new ApkId(body).save().then((app)=>{
        res.status(200).json({
            message: 'app saved to db'
        });
    },(err)=>{

        if (err.code === 11000){
            res.status(404).json({
                message: 'this app is exist'
            })
            return;
        }

        res.status(404).json({
            message: 'can not save app:'+err
        })
    })
};

let showApp = function(req, res){

    let page = req.query.page;
    let limit = req.query.limit;
    let search = req.query.search;

    if (page === undefined){
        page = 1;
    }

    if (limit ===  undefined){
        limit = 20
    }

    if(search === undefined){
        ApkId.paginate({}, {page: parseInt(page), limit: parseInt(limit)} ,function(error, result){

            if (error){
                res.status(404).json({
                    message: error
                })
                return;
            }

            res.status(200).send(result);
        })
    }else
    {
        ApkId.paginate({"appId": { "$regex": search, "$options": "i" }}, {page: parseInt(page), limit: parseInt(limit)} ,function(error, result){

            if (error){
                res.status(404).json({
                    message: error
                });
                return
            }
            res.status(200).send(result)
        })
    }


};


let getAppId = async function(){
    return ApkId.findOne({})
        .sort({reviewDate: 'ascending'})
        .then((res)=>{
            if (!res){
                return Promise.reject('can not find appId');
            }

            return res;

            // let q = queue();
            //
            // res.forEach(function (element) {
            //     q.push(element)
            // });
            //
            // return q;

        },(err)=>{
            return Promise.reject('can not find appId');
        });

};

module.exports = {
    deleteApp,
    createApp,
    showApp,
    getAppId
};