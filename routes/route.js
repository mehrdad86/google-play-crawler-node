const express = require('express');
const apkIdController = require('./controllers/apkIdController');

let router = express.Router();

router.route('/deleteapk/:appId').delete(apkIdController.deleteApp);
router.route('/addApk').post(apkIdController.createApp);
router.route('/showApk').get(apkIdController.showApp);




module.exports = router;