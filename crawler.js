const sleep = require('sleep');
let open = require('amqplib').connect('amqp://localhost:5672');

const apkIdController = require('./routes/controllers/apkIdController');
const getDetail = require('./service/getAppDetails');
const getReviews = require('./service/getAppReviews');
const utils = require('./utils/utils');

let q = 'detail';
let q1 = 'reviews';

let getApp = async function () {

    try {
        let appId = await apkIdController.getAppId();
        let detail = await getDetail(appId.appId);
        let reviews = await getReviews(appId.appId);

        let reviewsWithId = {
            reviews: reviews,
            appId: appId.appId
        };

        await utils.updateDate(appId.appId);


        open.then(function(conn) {
            return conn.createChannel();
        }).then(function(ch) {
            return ch.assertQueue(q).then(function(ok) {
                ch.sendToQueue(q, Buffer.from(JSON.stringify(detail)));
                return ch.close();
            });
        }).catch(console.warn);

        open.then(function(conn) {
            return conn.createChannel();
        }).then(function(ch) {
            return ch.assertQueue(q1).then(function(ok) {
                ch.sendToQueue(q1, Buffer.from(JSON.stringify(reviewsWithId)));
                return ch.close();
            });
        }).catch(console.warn);


        sleep.msleep(10000);
        getApp();

    }catch (e) {
        console.log('error:'+e)
        sleep.msleep(10000);
        getApp();
    }
};

getApp();


